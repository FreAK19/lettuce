import React, { Component, Fragment } from 'react';
import { StyleSheet, ActivityIndicator } from 'react-native';

import { StoreProvider } from './src/store';

import * as Font from 'expo-font';

import { Routes } from './src/Routes';

import { palette } from './src/utils/palette';

export default class App extends Component {
  state = {
    fontLoaded: false
  };

  async componentDidMount() {
    await Font.loadAsync({
      'montserrat-regular': require('./assets/fonts/Montserrat-Regular.ttf'),
      'montserrat-bold': require('./assets/fonts/Montserrat-Bold.ttf'),
    });
    this.setState({ fontLoaded: true });
  }

  render() {
    const { fontLoaded } = this.state;

    return (
      <Fragment>
        {
          !fontLoaded && (
            <ActivityIndicator
              size="large"
              color={palette.primary}
            />
          )
        }

        {
          fontLoaded ? (
            <StoreProvider>
              <Routes />
            </StoreProvider>
          ) : null
        }
      </Fragment>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    padding: 40
  }
});
