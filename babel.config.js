module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        require.resolve('babel-plugin-module-resolver'),
        {
          root: ['./src'],
          alias: {
            utils: './src/utils',
            components: './src/components',
            container: './src/container',
            i18n: './src/i18n',
          },
          cwd: 'packagejson'
        },
      ]
    ]
  };
};
