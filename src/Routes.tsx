import React from 'react';
import { StyleSheet, Text, Alert } from 'react-native';
import { Router, Scene, Stack } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { LoginPage, ConfirmPage, SignUpPage } from './container/auth';
import { GeneralSettingPage } from './container/general-setting';

import {
  AddImagePage,
  SettingPage,
  GallaryPage,
  ChooseImagePage,
  GlobalSettingPage
} from './container/portal';
import { routes } from './routing';
import { palette } from 'utils/palette';

const backIcon = () => (
  <Icon
    color="#fff"
    name="keyboard-arrow-left"
    size={20}
  />
);

const renderTitle = (title: string) => {
  return (
    <Text style={styles.pageTitle}>{title}</Text>
  )
};

export function Routes() {
  const {
    signIn,
    regConfirm,
    signUp,
    welcome,
    gallery,
    chooseImage,
    addPhoto,
    setting,
    globalSetting
  } = routes;

  return (
    <Router>
      <Stack
        key="root"
        navigationBarStyle={styles.container}
      >
        <Scene
          key={globalSetting}
          onLeft={() => {}}
          leftTitle="Back"
          rightTitle=" "
          onRight={() => {}}
          renderTitle={() => renderTitle('Настройки')}
          component={GlobalSettingPage}
        />
        <Scene
          key={chooseImage}
          onLeft={() => {}}
          leftTitle="Back"
          rightTitle=" "
          onRight={() => {}}
          renderTitle={() => renderTitle('Выберите источник')}
          component={ChooseImagePage}
          leftButtonTextStyle={styles.title}
        />
        <Scene
          key={addPhoto}
          onLeft={() => {}}
          leftTitle="Back"
          rightTitle=" "
          onRight={() => {}}
          renderTitle={() => renderTitle('Добавить фото')}
          component={AddImagePage}
          leftButtonTextStyle={styles.title}
        />
        <Scene
          key={setting}
          onLeft={() => {}}
          leftTitle=" "
          rightTitle=" "
          onRight={() => {}}
          renderTitle={() => renderTitle('Личный кабинет')}
          component={SettingPage}
        />
      </Stack>
    </Router>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: palette.primary
  },
  title: {
    color: palette.white
  },
  pageTitle: {
    flex: 1,
    fontFamily: palette.base.fontFamily,
    fontSize: 17,
    lineHeight: 22,
    color: '#fff',
    textAlign: 'center',
  }
});
