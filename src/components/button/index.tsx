import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  Text,
} from 'react-native';

import { palette, extraButtonStyle } from '../../utils/palette';

type Props = {
  style?: any,
  color?: string,
  title: string,
  variant?: string,
  onClick: () => void,
  disabled?: boolean,
  outlined?: boolean
};

export function Button ({
  title,
  onClick,
  disabled,
  style: classes = {},
  color = palette.white,
  variant = 'primary',
  outlined = false,
}: Props) {
  const buttonColor: string = (outlined && color) ? color : '#fff';
  const borderColor: string = outlined ? (color || palette[variant]) : 'transparent';

  const styles = StyleSheet.create({
    container: {
      borderRadius: 8,
      marginBottom: 10,
      borderWidth: 1,
      borderStyle: 'solid',
      backgroundColor: outlined ? 'transparent' : palette[variant],
      borderColor
    },
    text: {
      fontFamily: palette.base.bold,
      paddingHorizontal: 10,
      paddingVertical: 12,
      color: buttonColor,
      fontWeight: 'bold',
      letterSpacing: 1.1,
      textAlign: 'center',
      ...classes,
      ...extraButtonStyle[variant]
    }
  });

  const ButtonComponent = outlined ? TouchableHighlight : TouchableOpacity;

  return (
// @ts-ignore
  <ButtonComponent
    disabled={disabled}
    activeOpacity={0.8}
    style={styles.container}
    onPress={onClick}
    underlayColor='#fff'
  >
    <Text style={styles.text}>{title}</Text>
  </ButtonComponent>
  );
}
