import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import { palette } from '../../utils/palette';

type Props = {
  activeIndex: number,
  totalDots: number,
  onClick: (index: number) => void
};

const { pink, darkGrey } = palette;

export function CarouselDots({ activeIndex, totalDots, onClick }: Props) {
  const [active, setActive] = useState(0);

  const changeActiveDot = (index: number) => {
    setActive(index);
    onClick(index);
  };

  const Dot = ({ index }) => (
    <Text
      style={styles.dot}
      onPress={() => changeActiveDot(index)}
    >

    </Text>
  );

  return (
    <View style={styles.dots}>
      {
        new Array(totalDots).fill('dot').map((_item: string, index: number) => (
          <Dot index={index} key={index}/>
        ))
      }
    </View>
  );
}

const styles = StyleSheet.create({
  dots: {

  },
  dot: {
    width: 50,
    height: 50,
    backgroundColor: darkGrey,
    borderRadius: 25
  }
});
