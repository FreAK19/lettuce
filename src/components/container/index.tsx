import React from 'react';
import { View, StyleSheet } from 'react-native';

import { palette } from 'utils/palette';

type Props = {
  classes?: any,
  children: any
};

export function Container({ children, classes }: Props) {
  return (
    <View
      style={{ ...styles.container, ...classes }}
    >
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: palette.lightGrey,
    paddingHorizontal: 24
  }
});
