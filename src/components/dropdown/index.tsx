import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { Dropdown as GlobalDropdown } from 'react-native-material-dropdown';

import { palette } from '../../utils/palette';

const { white, lightGrey, yellow, fontColor } = palette;

type Props = {
  data: any[],
  label: string,
  value?: string
}

export function Dropdown({ label, data, value }: Props) {

  const renderLabel = ({ label }) => {
    return <Text>{label}</Text>;
  };

  return (
    <GlobalDropdown
      label={label}
      data={data}
      placeholderTextColor={yellow}
      animationDuration={100}
      textColor={fontColor}
      baseColor={fontColor}
      itemPadding={15}
      value={value}
      selectedItemColor={yellow}
    />
  );
}

const styles = StyleSheet.create({
  picker: {
    color: yellow
  },
  container: {
    flex: 1,
    backgroundColor: lightGrey
  }
});
