import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { palette } from './../../utils/palette';

type Props = {
  title: string
}

export function NavBar ({ title }: Props) {
  return (
    <View>
    </View>
  )
}

const styles = StyleSheet.create({
  toolbar: {
    height: 70,
    position: 'relative',
    color: palette.white,
    backgroundColor: palette.primary,
  },
  backarrowStyle: {
    width: 12,
    height: 20,
    left: 0,
    bottom: 0,
    position: 'absolute',
  },
  title: {
    fontWeight: '700',
    fontSize: 17,
    lineHeight: 22,
    textAlign: 'center',
    color: palette.white,
    position: 'absolute',
    bottom: 10,
    left: 0,
    right: 0,
    fontFamily: palette.base.bold
  }
});
