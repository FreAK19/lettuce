import React from 'react';
import Toggle from 'toggle-switch-react-native';

import { palette } from 'utils/palette';

type Props = {
  checked: boolean,
  onChange: (checked: boolean) => void
};

export function Switch({ checked = false, onChange }: Props) {

  return (
    <Toggle
      isOn={checked}
      size="large"
      onColor={palette.yellow}
      offColor={palette.darkGrey}
      onToggle={checked => onChange(checked)}
    />
  );
}
