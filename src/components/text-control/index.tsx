import React from 'react';

import { palette } from '../../utils/palette';

import { TextField } from 'react-native-material-textfield';

type Props = {
  value?: string,
  label: string,
  name?: string,
  keyboard?: string,
  baseColor?: string,
  textColor?: string,
  multiline?: boolean,
  disabled?: boolean,
  onSubmit?: () => void
  onChange?: (field: string) => void
}

const { fontColor, yellow } = palette;

export function TextControl ({
  label,
  name,
  onChange,
  value = '',
  textColor = fontColor,
  baseColor = fontColor,
  onSubmit = () => {},
  keyboard = 'default',
  disabled,
  multiline
}: Props) {

  return (
    <TextField
      name={name}
      value={value}
      label={label}
      tintColor={yellow}
      multiline={multiline}
      disabled={disabled}
      textColor={textColor}
      baseColor={baseColor}
      keyboardType={keyboard}
      onChangeText={onChange}
      onSubmitEditing={onSubmit}
    />
  )
}