import React from 'react';
import { FlatList, StyleSheet, Text } from 'react-native';

import { palette } from '../../utils/palette';

type Props = {
  list: any[],
  styles?: any
}

export function UnorderList({ list, styles = {} }: Props) {

  const Item = ({ title }) => {
    return (
      <Text style={innerStyles.listItem}>{`\u2022 ${title}`}</Text>
    );
  };

  return (
    <FlatList
      data={list}
      style={styles}
      renderItem={({ item }) => <Item title={item.title} />}
      keyExtractor={item => item.id}
    />
  );
}

const innerStyles = StyleSheet.create({
  listItem: {
    fontFamily: palette.base.fontFamily,
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 20,
    marginBottom: 20
  }
});