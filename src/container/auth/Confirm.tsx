import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Alert,
  ScrollView
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import KeyCodeInput from '@twotalltotems/react-native-otp-input'

import { Container, Logo, Button } from '../../components';

import { routes } from '../../routing';
import { palette } from '../../utils/palette';

const { fontColor, yellow, lightGrey, base } = palette;

export function ConfirmPage() {
  const [code, setCode]: [string, Function] = useState('');

  return (
    <Container classes={styles.container}>
      <ScrollView>
        <Logo />
        <View>
          <Text style={styles.title}>Введите СМС-код</Text>
          <View style={styles.codeInputWrapper}>
            <KeyCodeInput
              style={styles.input}
              pinCount={4}
              code={code}
              autoFocusOnLoad={false}
              onCodeChanged={(code: string) => setCode(code)}
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              onCodeFilled = {(code => {
                Alert.alert(`Code is ${code}, you are good to go!`)
              })}
            />
          </View>

          <Button
            onClick={() => Actions.push(routes.welcome)}
            style={styles.button}
            title="Отправить повторно"
            variant="default"
            outlined
            color="rgba(0, 0, 0, 0.12)"
          />
        </View>
      </ScrollView>
    </Container>
  );
}

const styles = StyleSheet.create({
  input: {
    width: '90%',
    height: 45,
    fontSize: 24,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 1,
    color: fontColor,
    fontWeight: '500',
  },
  codeInputWrapper: {
    fontSize: 24,
    paddingTop: 50,
    paddingBottom: 22,
    alignItems: 'center'
  },
  underlineStyleBase: {
    width: 60,
    height: 50,
    borderWidth: 0,
    borderColor: yellow,
    borderBottomWidth: 2,
  },
  borderStyleHighLighted: {
    borderColor: yellow,
  },
  underlineStyleHighLighted: {
    borderColor: yellow,
  },
  title: {
    marginTop: 35,
    fontFamily: base.fontFamily,
    fontSize: 16,
    textAlign: 'center',
    color: 'rgba(0, 0, 0, .8)',
  },
  button: {
    color: 'rgba(0, 0, 0, .8)',
  },
  container: {
    textAlign: 'center',
    paddingTop: 70,
    paddingBottom: 40,
    flex: 1,
    backgroundColor: lightGrey
  }
});
