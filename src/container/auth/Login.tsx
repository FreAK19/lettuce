import React from 'react';
import {
  View,
  ImageBackground,
  StyleSheet,
  Text,
  Linking,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import Svg, { Path, Circle } from 'react-native-svg'
import { Actions } from 'react-native-router-flux';

import { Container, Logo, Button, TextControl } from '../../components';
import { links } from '../../utils/constants';
import { palette } from '../../utils/palette';

import { routes } from '../../routing';

const { fb, insta, privacy, vk } = links;

export function LoginPage() {

  const renderVkIcon  = () => (
    <View style={styles.socialIcon}>
      <TouchableOpacity
        activeOpacity={.6}
        onPress={() => Linking.openURL(vk)}
      >
        <Svg width={50} height={50} fill="none">
          <Circle cx={25} cy={25} r={24.5} stroke="#fff" />
          <Path
            fill="#fff"
            d="M36.867 30.882a1.732 1.732 0 00-.084-.163c-.434-.781-1.263-1.74-2.486-2.877l-.026-.026-.013-.013-.013-.013h-.013c-.556-.53-.907-.885-1.055-1.067-.269-.348-.33-.699-.182-1.055.104-.269.495-.837 1.171-1.705.356-.46.638-.828.846-1.106 1.502-1.996 2.153-3.272 1.953-3.827l-.078-.13c-.052-.078-.186-.15-.403-.215-.217-.065-.495-.075-.833-.032l-3.749.026a.486.486 0 00-.26.006l-.17.04-.065.032-.052.04a.579.579 0 00-.143.136.892.892 0 00-.13.228 21.26 21.26 0 01-1.393 2.928 27.754 27.754 0 01-.885 1.4c-.269.394-.494.685-.677.872a4.712 4.712 0 01-.494.449c-.148.113-.26.16-.339.143a9.328 9.328 0 01-.221-.052.865.865 0 01-.293-.32 1.429 1.429 0 01-.15-.507 5.569 5.569 0 01-.045-.527 11.77 11.77 0 01.006-.625c.01-.269.014-.451.014-.547 0-.33.006-.687.019-1.073l.033-.918c.008-.226.012-.464.012-.716a3.03 3.03 0 00-.045-.592 2.026 2.026 0 00-.136-.417.701.701 0 00-.267-.312 1.503 1.503 0 00-.436-.176c-.46-.104-1.046-.16-1.758-.17-1.614-.017-2.65.088-3.11.313a1.755 1.755 0 00-.495.39c-.156.192-.178.296-.065.313.52.078.889.265 1.106.56l.078.156c.06.113.122.313.182.599.061.286.1.603.117.95a9.973 9.973 0 010 1.627c-.043.451-.084.803-.123 1.054-.04.252-.098.456-.176.612-.078.156-.13.252-.156.286a.223.223 0 01-.065.065.973.973 0 01-.352.066c-.121 0-.269-.061-.442-.183a3.12 3.12 0 01-.54-.5 6.702 6.702 0 01-.632-.893 15.563 15.563 0 01-.729-1.353l-.208-.378a32.37 32.37 0 01-.533-1.06 20.461 20.461 0 01-.6-1.348.858.858 0 00-.311-.416l-.066-.04a.888.888 0 00-.208-.11 1.386 1.386 0 00-.3-.085l-3.566.026c-.364 0-.611.083-.742.248l-.052.078a.42.42 0 00-.039.208c0 .096.026.213.078.351a42.985 42.985 0 001.699 3.54c.611 1.138 1.143 2.053 1.594 2.747a32.3 32.3 0 001.38 1.966c.468.616.779 1.01.93 1.184.152.174.272.304.359.39l.325.313c.208.208.514.458.918.748.403.291.85.578 1.34.86.49.282 1.061.512 1.712.69.65.177 1.284.249 1.9.214h1.498c.303-.026.533-.121.69-.286l.051-.065a.864.864 0 00.098-.24c.03-.11.045-.229.045-.359a4.29 4.29 0 01.085-1.008c.065-.3.138-.525.221-.677a1.662 1.662 0 01.502-.573.863.863 0 01.103-.045c.209-.07.454-.003.736.201.282.204.547.456.794.755.247.3.545.636.892 1.01.347.372.65.65.91.832l.261.156c.174.105.4.2.677.287.277.087.52.108.729.065l3.332-.052c.33 0 .586-.055.768-.163.182-.108.29-.228.325-.358.035-.13.037-.277.007-.443a1.636 1.636 0 00-.092-.344z"
          />
        </Svg>
      </TouchableOpacity>
    </View>
  );


  const renderInstaIcon  = () => (
    <View style={styles.socialIcon}>
      <TouchableOpacity
        activeOpacity={.6}
        onPress={() => Linking.openURL(vk)}
      >
        <Svg viewBox="0 0 50 50">
          <Path d="M258.9 507.2C120.4 507.9 6.6 392.6 9.9 252 12.9 118 124 7 262.3 8.7c136.6 1.7 249.4 115.4 245.8 256-3.4 133.5-113.4 243.1-249.2 242.5zM40.2 257.9c.9 122.6 97.9 218.2 214.4 219.9 123.6 1.8 222.8-95.7 223.1-219.5.1-122.7-97.8-218-214.5-220.3-120.1-2.3-222 94.3-223 219.9z" />
          <Path d="M413.2 261.3c0 21.8-.6 43.7.1 65.5 1.5 41.9-28.7 76.9-68.6 86.9-7.8 2-16.1 3.2-24.1 3.3-42.3.3-84.6.7-126.9 0-35.3-.6-63.1-15.6-81.9-46.1-7.2-11.7-10.8-24.7-10.8-38.5-.1-47.5-.5-95 .1-142.4.4-31 15.9-53.9 41.6-70.3 16-10.2 33.7-14.8 52.6-14.8 41.3-.1 82.6-.5 123.9.1 36.4.5 65.2 15.7 83.9 47.7 6.7 11.5 10.1 24.2 10.1 37.7-.1 23.6 0 47.3 0 70.9zm-289.2-.2v68.5c0 9.1 1.7 17.9 6 26 13.8 26 36.4 37.8 64.9 38.2 41.3.6 82.7.3 124 0 7.1 0 14.3-1.2 21.2-3 27.5-7.4 50.9-31.2 50.1-66.1-1-44-.2-88-.2-132 0-9.7-1.8-18.9-6.5-27.4-14.1-25.3-36.6-36.8-64.6-37.2-41.2-.6-82.3-.4-123.5 0-8.2.1-16.6 1.6-24.5 4-26.9 8.3-48.1 32.2-47 64.4.7 21.6.1 43.1.1 64.6z" />
          <Path d="M257.1 183c40.7-.1 73.7 32.4 73.7 72.4-.1 39.8-33 72.3-73.3 72.4-40.6.1-73.5-32.3-73.5-72.5 0-39.9 32.6-72.2 73.1-72.3zm0 23.2c-27.4.1-50 22.4-49.9 49.3s22.6 49 50.2 49.1c27.5 0 50.1-22.1 50.2-49-.1-27.3-22.8-49.5-50.5-49.4zm97.7-29.5c.2 8.5-6.8 16-15.1 16.3-8.5.3-15.8-6.9-15.9-15.8-.1-8.7 6.6-16 14.9-16.3 8.5-.3 15.9 7 16.1 15.8z" />
        </Svg>
      </TouchableOpacity>
    </View>
  );

  const renderLastIcon  = () => (
    <View>
      <TouchableOpacity
        activeOpacity={.6}
        onPress={() => Linking.openURL(vk)}
      >
        <Svg width={50} height={50} fill="none">
          <Circle cx={25} cy={25} r={24.5} stroke="#fff" />
          <Path
            fill="#fff"
            d="M7.333 7.333v-2.2c0-.953.22-1.466 1.76-1.466H11V0H8.067C4.4 0 2.933 2.42 2.933 5.133v2.2H0V11h2.933v11h4.4V11h3.227L11 7.333H7.333z"
          />
        </Svg>
      </TouchableOpacity>
    </View>
  );

  return (
    <ImageBackground
      source={require('../../../assets/images/auth/bg.png')}
      style={styles.background}
    >
      <StatusBar hidden />
      <Container classes={styles.container}>
        <Logo />
        <View>
          <TextControl
            textColor={palette.white}
            baseColor="rgba(255, 255, 255, .7)"
            label="Введите номер телефона"
          />

          <Button
            onClick={() => Actions.push(routes.setting)}
            title="Войти"
            variant="primary"
          />
          <Button
            onClick={() => Actions.push(routes.signUp)}
            title="Зарегистрироваться"
            variant="default"
            outlined
            color={palette.white}
          />
        </View>
        <View>
          <View style={styles.social}>
            {renderVkIcon()}
            {renderInstaIcon()}
            {renderLastIcon()}
          </View>
          <View style={styles.privacy}>
            <TouchableOpacity>
              <Text
                style={styles.privacyPolicy}
                onPress={() => Linking.openURL(privacy)}>
                Политика конфиденциальности
              </Text>
            </TouchableOpacity>
            <Text style={styles.privacyPolicy}>
              и
            </Text>
            <TouchableOpacity>
              <Text
                style={styles.privacyPolicy}
                onPress={() => Linking.openURL(privacy)}>
                Лицензионное соглашение
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Container>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
  social: {
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20
  },
  socialIcon: {
    marginRight: 16
  },
  container: {
    textAlign: 'center',
    paddingTop: 70,
    paddingBottom: 40,
    flex: 1,
    justifyContent: 'space-between'
  },
  privacy: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  privacyPolicy: {
    fontFamily: palette.base.fontFamily,
    fontWeight: '500',
    fontSize: 10,
    lineHeight: 18,
    textAlign: 'center',
    textDecorationLine: 'underline',
    color: 'rgba(255, 255, 255, .5)'
  }
});
