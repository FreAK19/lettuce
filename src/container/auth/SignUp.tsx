import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { NavBar } from '../../components';

import { Container, Logo, Button, TextControl } from '../../components';

import { routes } from '../../routing';
import { palette } from '../../utils/palette';

export function SignUpPage() {
  const [phone, setPhone] = useState('');

  return (
    <Container classes={styles.container}>
      <NavBar title="Hello"/>
      <Logo />
      <View style={styles.form}>
        <TextControl
          value={phone}
          keyboard="phone-pad"
          onChange={(phone: string) => setPhone(phone)}
          label="Введите номер телефона"
        />

        <Button
          onClick={() => Actions.push(routes.regConfirm)}
          title="Зарегистрироваться"
          variant="primary"
        />
      </View>
    </Container>
  );
}

const styles = StyleSheet.create({
  form: {
    paddingTop: 100,
    backgroundColor: palette.lightGrey,
  },
  container: {
    textAlign: 'center',
    paddingTop: 70,
    paddingBottom: 40,
    flex: 1,
    backgroundColor: palette.lightGrey,
  }
});
