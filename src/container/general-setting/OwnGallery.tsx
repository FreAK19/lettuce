import React from 'react';
import { StyleSheet, View, Text, ScrollView, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

import { Button } from 'components/button';

import { palette } from 'utils/palette';

function GalleryItem() {
  return (
    <View style={styles.rectStack}>
      <View style={styles.rect}>
        <View style={styles.ellipse}>
          <Icon name="plus" style={styles.icon} size={30} />
        </View>
      </View>
    </View>
  )
}

export function OwnGallery() {
  return (
    <ScrollView style={{ flex: 1 }}>
      <View style={styles.container}>
        <Text style={styles.title}>
          Добавьте Ваше первое фото, чтобы начать обмениваться вещами
        </Text>
        <View style={styles.gallery}>
          <View style={styles.rectStackRow}>
            <GalleryItem />
            <GalleryItem />
            <GalleryItem />
          </View>
          <View style={styles.rectStackRow}>
            <GalleryItem />
            <GalleryItem />
            <GalleryItem />
          </View>
          <View style={styles.rectStackRow}>
            <GalleryItem />
            <GalleryItem />
            <GalleryItem />
          </View>
        </View>
        <View style={styles.actions}>
          <Button
            title="Пропустить"
            variant="default"
            outlined
            color={palette.primary}
            onClick={() => {}}
          />
          <Button
            onClick={() => {}}
            title="Далле"
            variant="primary"
          />
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 24,
    paddingHorizontal: 15,
    backgroundColor: '#fff'
  },
  actions: {
    marginBottom: 50
  },
  gallery: {
    paddingVertical: 23
  },
  title: {
    textAlign: 'center',
    fontFamily: palette.base.fontFamily,
    fontSize: 16,
    lineHeight: 18
  },
  rect: {
    width: 100,
    height: 119,
    borderRadius: 4,
    position: 'relative',
    backgroundColor: palette.lightGrey,
  },
  icon: {
    color: '#fff',
    zIndex: 7,
    fontSize: 20,
    fontWeight: 'bold',
    elevation: Platform.OS === 'android' ? 7 : 0,
    textAlign: 'center',
    marginTop: 5,
  },
  ellipse: {
    top: 96,
    right: -10,
    width: 30,
    height: 30,
    borderRadius: 15,
    position: 'absolute',
    backgroundColor: palette.yellow,
    shadowColor: 'rgba(0, 0, 0, 0.04)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
  },
  rectStack: {
    width: 100,
    height: 119,
    marginBottom: 14
  },
  rectStackRow: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
});
