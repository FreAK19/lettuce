import React from 'react';
import { View, StyleSheet, Text, StatusBar } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Logo, Button, TextControl } from '../../components';

import { routes } from '../../routing';
import { palette } from 'utils/palette';

type Props = {
  next: () => void
}

export function UserEmailPage({ next }: Props) {

  return (
    <Container classes={styles.container}>
      <StatusBar animated hidden={false} backgroundColor={palette.primary} />
      <Logo />
      <View>
        <Text style={styles.title}>Введите ваш email</Text>

        <TextControl
          keyboard="email-address"
          label="Email"
        />

        <Button
          onClick={() => Actions.push(routes.chooseImage)}
          title="Далле"
          variant="primary"
        />
      </View>
    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    textAlign: 'center',
    paddingTop: 70,
    paddingBottom: 40,
    flex: 1,
  },
  title: {
    fontFamily: palette.base.fontFamily,
    marginTop: 15,
    fontSize: 16,
    textAlign: 'center',
    color: 'rgba(0, 0, 0, .8)',
  }
});
