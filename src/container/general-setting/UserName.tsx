import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

import { Container, Logo, Button, TextControl } from '../../components';

import { palette } from '../../utils/palette';

type Props = {
  next: () => void
}

export function UserNamePage({ next }: Props) {

  return (
    <Container classes={styles.container}>
      <View style={styles.page}>
        <View>
          <Logo />
          <View>
            <Text style={styles.title}>Введите ваше имя</Text>
            <TextControl
              label="Имя пользователя"
            />
          </View>
        </View>

        <Button
          onClick={next}
          title="Далле"
          variant="primary"
        />
      </View>
    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    textAlign: 'center',
    paddingTop: 70,
    paddingBottom: 40,
    flex: 1,
  },
  page: {
    height: '100%',
    flex: 1,
  },
  title: {
    fontFamily: palette.base.fontFamily,
    marginTop: 15,
    fontSize: 16,
    textAlign: 'center',
    color: 'rgba(0, 0, 0, .8)',
  }
});
