import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  ScrollView
} from 'react-native';

import { Container, Logo, Button, UnorderList } from '../../components';

import { palette } from '../../utils/palette';

const { lightGrey } = palette;

export function WelcomePage() {

  const data: any[] = [
    {
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba'
    },
    {
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53tbb28ba'
    },
    {
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
      id: 'bd7acbea-c1b1-46c2-aed5-3ad73abb28ba'
    },
    {
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
      id: 'bd7acbea-c1b1-46c2-aed5-3ad59abb28ba'
    }
  ];

  return (
    <ScrollView>
      <Container classes={styles.container}>
        <Logo />
        <View>
          <Text style={styles.title}>Добро пожаловать в Lettuce</Text>

          <UnorderList list={data}/>

          <Button
            onClick={() => {}}
            title="Далее"
            variant="primary"
          />
        </View>
      </Container>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    lineHeight: 29,
    color: '#000',
    paddingTop: 40,
    paddingBottom: 20,
    textAlign: 'center',
    fontFamily: palette.base.fontFamily
  },
  container: {
    textAlign: 'center',
    paddingTop: 70,
    paddingBottom: 40,
    flex: 1,
    backgroundColor: lightGrey
  }
});
