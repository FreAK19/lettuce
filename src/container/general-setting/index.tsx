import React, { useState } from 'react';
import { StyleSheet, View, SafeAreaView, Dimensions } from 'react-native';

import Carousel from 'react-native-snap-carousel';

import { WelcomePage } from './Welcome';
import { UserNamePage } from './UserName';
import { UserEmailPage } from './UserEmail';
import { OwnGallery } from './OwnGallery';

import { CarouselDots } from '../../components/carousel-dots';

import { palette } from '../../utils/palette';

export function GeneralSettingPage() {
  const [activeIndex, setActiveIndex] = useState(0);
  const { width, height } = Dimensions.get('window');
  let carouselRef = null;

  const changeActiveScreen = (index: number) => {
    if (carouselRef) {
      //  @ts-ignore
      carouselRef._snapToItem(index);
    }
  };

  const carouselItems = [
    {
      component: <WelcomePage />
    },
    {
      component: <UserNamePage next={() => changeActiveScreen(activeIndex + 1)} />
    },
    {
      component: <UserEmailPage next={() => changeActiveScreen(activeIndex + 1)}/>
    },
    {
      component: <OwnGallery />
    },
  ];

  const renderItem = ({ item }) => {
    return (
      <View style={{ height }}>
        {item.component}
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <Carousel
        ref={ref => carouselRef = ref}
        data={carouselItems}
        sliderWidth={width}
        itemWidth={width}
        acti
        onSnapToItem={index => setActiveIndex(index)}
        renderItem={renderItem}
      />

      <CarouselDots
        activeIndex={activeIndex}
        totalDots={carouselItems.length}
        onClick={(index: number) => setActiveIndex(index)}
      />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: palette.lightGrey
  },
});
