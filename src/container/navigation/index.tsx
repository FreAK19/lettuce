import React from 'react';
import { View, StyleSheet } from 'react-native';
import
  BottomNavigation, {
  FullTab
} from 'react-native-material-bottom-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { palette } from '../../utils/palette';

const { lightGrey, primary, lightColor } = palette;
const barColor: string = lightGrey;

type Props = {
  children: any,
  activeTab: string,
  onChange: (key: string) => void,
};

type NavigationItem = {
  key: string,
  icon: string,
  label: string,
  barColor: string,
};

const tabs: NavigationItem[] = [
  {
    key: 'account',
    icon: 'account-circle',
    label: 'Личный кабинет',
    barColor,
  },
  {
    key: 'dashboard',
    icon: 'dashboard',
    label: 'Библиотека',
    barColor,
  },
  {
    key: 'person',
    icon: 'person-pin',
    label: 'Активность',
    barColor,
  }
];

export class Navigation extends React.Component<Props> {
  renderIcon = icon => ({ isActive }) => (
    <Icon
      size={24}
      color={isActive ? primary : lightColor }
      name={icon}
    />
  );

  renderTab = ({ tab, isActive }) => (
    <FullTab
      isActive={isActive}
      key={tab.key}
      label={tab.label}
      labelStyle={{ color: isActive ? primary : lightColor }}
      renderIcon={this.renderIcon(tab.icon)}
    />
  );

  render() {
    const { activeTab, children, onChange } = this.props;

    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          {children}
        </View>
        <BottomNavigation
          activeTab={activeTab}
          useLayoutAnimation={false}
          style={styles.navigations}
          onTabPress={(newTab: NavigationItem) => onChange(newTab.key)}
          renderTab={this.renderTab}
          tabs={tabs}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  navigations: {
    paddingBottom: 10
  }
});
