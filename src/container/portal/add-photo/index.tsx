import React from 'react';
import { Actions } from 'react-native-router-flux';
import {
  View,
  StyleSheet,
  Image,
  ScrollView
} from 'react-native';
import {
  Container,
  TextControl,
  Dropdown,
  Button
} from '../../../components';

import { routes } from '../../../routing';
import { connect } from '../../../store/connect';
import { palette } from 'utils/palette';

const { lightGrey } = palette;

type Props = {
  currentDownloadedImageUrl: string
};

function AddImage({ currentDownloadedImageUrl }: Props) {

  let data = [{
    value: 'Banana',
  }, {
    value: 'Mango',
  }, {
    value: 'Pear',
  }];

  return (
    <ScrollView style={styles.scroll}>
      <Container classes={styles.container}>
        <View style={styles.imagePreview}>
          <Image
            style={styles.image}
            borderRadius={10}
            source={{ uri: currentDownloadedImageUrl }}
          />
        </View>
        <View>
          <TextControl label="Заголовок"/>

          <Dropdown
            label="Категория"
            data={data}
          />

          <Dropdown
            label="Размер"
            data={data}
          />

          <TextControl
            label="Комментарий"
            keyboard="default"
            multiline
            value="orem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
          />
        </View>

        <View style={styles.action}>
          <Button
            title="Добавить"
            onClick={() => Actions.push(routes.setting)}
          />
        </View>
      </Container>
    </ScrollView>
  );
}

export const AddImagePage = connect(AddImage, {
  store: ['currentDownloadedImageUrl'],
  dispatch: {}
});

const styles = StyleSheet.create({
  imagePreview: {
    overflow: 'hidden',
  },
  image: {
    width: '100%',
    height: 250,
  },
  action: {
    marginTop: 20
  },
  scroll: {
    backgroundColor: lightGrey
  },
  container: {
    flex: 1,
    paddingTop: 20,
    paddingBottom: 80,
  }
});
