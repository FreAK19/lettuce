import React, { useEffect, useState } from 'react';
import Constants from 'expo-constants';
import Svg, { Path } from 'react-native-svg'
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import { Actions } from 'react-native-router-flux';
import { View, StyleSheet, TouchableOpacity, Alert, ActivityIndicator } from 'react-native';

import { Container } from 'components/container';

import { savePickerImage } from '../../../store/actions/ui';

import { palette } from 'utils/palette';
import { connect } from '../../../store/connect';
import { routes } from '../../../routing';

type Props = {
  savePickerImage: (uri: string) => void
};

const { white, lightGrey } = palette;

function ChooseImage({ savePickerImage }: Props) {
  const [loading, setLoading]: [boolean, Function] = useState(false);

  useEffect(() => {
    getPermissionAsync();
  }, []);

  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        Alert.alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  };

  const saveUrlToStore = (uri: string) => {
    setLoading(true);
    savePickerImage(uri);
    Actions.push(routes.addPhoto);
  };

  const pickImage = async (useCamera: boolean = false) => {
    const pickerImageAsync: Function = useCamera ? ImagePicker.launchCameraAsync : ImagePicker.launchImageLibraryAsync;
    let result = await pickerImageAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      quality: 1
    });

    if (!result.cancelled) {
      saveUrlToStore(result.uri);
    }
  };

  return (
    <Container classes={styles.container}>
      {
        loading ? (
          <ActivityIndicator
            size="large"
            color={palette.primary}
          />
        ) : (
          <View style={styles.images}>
            <TouchableOpacity
              activeOpacity={.7}
              onPress={() => pickImage()}
            >
              <View style={{ ...styles.image, marginRight: 30 }}>
                <Svg width={54} height={54} fill="none" style={styles.icon}>
                  <Path
                    fill="#000"
                    d="M4.904 45.235V9.884H.95a.952.952 0 00-.951.95v39.041c0 .527.428.955.95.955h45.124a.955.955 0 00.953-.955v-3.687H5.858a.952.952 0 01-.954-.953z"
                  />
                  <Path
                    fill="#000"
                    d="M53.046 3.172H7.928a.951.951 0 00-.95.95v39.04c0 .526.425.958.95.958h45.12c.524 0 .952-.43.952-.959V4.123a.955.955 0 00-.954-.951zm-33.43 3.55a5.062 5.062 0 015.06 5.062c0 2.8-2.268 5.057-5.06 5.057a5.054 5.054 0 01-5.06-5.057 5.061 5.061 0 015.06-5.063zm31.289 25.607H10.594v-.389l7.954-9.088 4.476 3.019 5.216-8.939 6.06 4.297 5.477-11.353 11.128 22.03v.423z"
                  />
                </Svg>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => pickImage(true)}
              activeOpacity={.7}
            >
              <View style={styles.image}>
                <Svg width={54} height={54} fill="none" style={styles.icon}>
                  <Path fill="#000" d="M27 34.2a7.2 7.2 0 100-14.4 7.2 7.2 0 000 14.4z" />
                  <Path
                    fill="#000"
                    d="M20.25 4.5L16.133 9H9a4.513 4.513 0 00-4.5 4.5v27C4.5 42.975 6.525 45 9 45h36c2.475 0 4.5-2.025 4.5-4.5v-27c0-2.475-2.025-4.5-4.5-4.5h-7.133L33.75 4.5h-13.5zM27 38.25c-6.21 0-11.25-5.04-11.25-11.25S20.79 15.75 27 15.75 38.25 20.79 38.25 27 33.21 38.25 27 38.25z"
                  />
                </Svg>
              </View>
            </TouchableOpacity>
          </View>
        )
      }
    </Container>
  );
}

export const ChooseImagePage = connect(ChooseImage, {
  store: [],
  dispatch: {
    savePickerImage
  }
});

const styles = StyleSheet.create({
  images: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    padding: 40,
    borderRadius: 8,
    backgroundColor: lightGrey
  },
  icon: {
    opacity: .5,
  },
  container: {
    flex: 1,
    marginHorizontal: 0,
    backgroundColor: white
  }
});
