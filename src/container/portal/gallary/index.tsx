import React, { useState } from 'react';
import {StyleSheet, View, Image, Text, Dimensions, ImageBackground} from 'react-native';

import Carousel from 'react-native-snap-carousel';

import { Navigation } from '../../navigation';

import { palette } from '../../../utils/palette';

const { white, pink, base } = palette;

export function GallaryPage() {

  const renderItem = ({ item }) => {
    return (
      <View style={styles.wrapper}>
        <ImageBackground
          source={{ uri: item.src }}
          style={styles.card}
        >
          <Text style={styles.title}>{ item.title }</Text>
        </ImageBackground>
      </View>
    );
  };

  const entries = [
    {
      title: 'Платье черное ',
      src: 'https://www.lulus.com/images/product/xlarge/3099630_391492.jpg?w=560'
    },
    {
      title: 'Платье черное ',
      src: 'https://img.ltwebstatic.com/images2_pi/2019/09/23/15692302792554727863_thumbnail_405x552.webp'
    },
    {
      title: 'Платье черное ',
      src: 'https://img.ohpolly.com/elKPdhixdHL9huRHolbOLRxzf4Q=/fit-in/1800x/catalog/product/1/6/1693_blush_37592_edit9-web.jpg'
    }
    ,{
      title: 'Платье черное ',
      src: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8QEhMSEA8QEBASEA8SDxUPEg8PDxUQFRIWFhcRFRYYHSggGBolGxUVITEhJSkrLi4vFx8zODMuNygtLisBCgoKDg0OGxAQGC0lHiUtLS0tLS0vLS0tLS0tLystLS0tLS0tKy0tLS0tLS4tLS0rLS0uLS0tLS0tLS0tLS0rLf/AABEIAQMAwwMBEQACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABwECAwQGBQj/xABEEAABAwEFBAcFBAcHBQAAAAABAAIDEQQFEiExBkFRcQcTImGBkbEUMnKhwSNSgtFic5KissLwJDNCU5Ph8TRDhKPD/8QAGwEBAAIDAQEAAAAAAAAAAAAAAAECAwQGBQf/xAA4EQEAAgECAwQIBQMDBQAAAAAAAQIDETEEEiEFMkFREyJhcYGRobEzwdHh8AYUQiNy8RU0UmKC/9oADAMBAAIRAxEAPwCcUBAQEBAQEBAQcn0qNJuu1U1DYvLrmV+VVFtlqbvmiQkUc0lrmuaWkag01HiFCdusO+2VvqW2BrGyFlojacTAWDEKgdYwuBI7wD/vq3pye56ODLF993ZEdQz7SRz5X5kl7pHfCKk0HcsMs2+zg9urA+VomAqYsRNPumlfRZsNtJ0a2enNGsMfRDf4sl4RhxpFaf7PJU0Ac51Y3ftho5PK2vFpadH0mrMYgICAgICAgICAgICAgICAgICDn9vbOZbBaGDVzYwP9RqrbZek6WfL9pZSo0pnQ9xzHmFEStMM9zShri7tBxaGtLCWkHG11ajMZNJ8O9J2K76w7247HKWtdjc8PIze4ud5ladtNXo0tOjsY7A0ihAoRnXRVhEy568ujqyTVMT3WaStax9pteOHd4UWWuS0MdsdZd7YtubHG1kU8+OVrGtlkjje6IvAoXZVpU50FaLLGes7slexuLtXnrTp7ZiJ+UupslqjlYHxPbIx3uuYQ5p8Qs0Tr1h5uTHbHaa3iYmPCWZSoICAgICAgICAgICAgICAgIPO2iextmnL3Na0RPNXEAYgOz86KJnSFqVm1oiIfLF5zNdJI8e66R5byL/yVIZZWXI0dYA6oYCcZaKupSlB5/1RVyW5YbHCcNfPflqkezbRQRMYyOCVzWD/ABOYwk8xVaU2dBTsS/8AleI+c/o2nbaGnZskYPGSV8oHfTCPVOb2M9ew669ck/CNPzl414X3arQC2SU9XvYwCOPkQNRzqomdXo4OBwYetK9fOes/t8GgH0yRtvb2Y2pmsMgLSXwvP2sROTqf4m8H9+/Q92THkmk+x5naHZ+Pi66T0t4T+U+z7eHtm+77bHPGyWJ2KORoc093A8Dup3LfiYmNYcJlxXxXml40mOkthSxiAgICAgICAgICAgICAgIIX6TNqzanGGPE2GFz61Ba58wq2rm7gDWgPGq1MmTntpGzqOG4D+14W2a/W1q+HhzdI+evX5Intev4yPUeoWeHPbzpD3LvsYYOJO/vWle82l3PAcHTBTo32hY3owvARbRVEtcv1PepY9d18cT3YMLXOOfugu8TRGHJmx005rRHvmHfdE1+yde6yNY+SFwfIXD3YXt1JrucaCnGnetnh5mJ5XPdv48OSlc9Z9bb/dH7fb4JYW25YQEBAQEBAQEBAQEBAQEGK0zCNjnkOIY1ziGgucQBWgAzJ7gomdFqV5rRXz83zbf9vdK6WZ/vyyFzhwJdXD4aeS0cfrX1dj2ty8NwdcNZ8oj2xHj/ADxeDZrMZJASNCCeYqR9VmyX0ro8XsvhJzZ4tO0Tr8d/574dCGrTdxELgETC5F2F87PvtrzCnRinJSPGGe6rsltbniAB2GmIlwYASNM9VOktLPx+DFOlpn4Q7a67udBGyN2o96mlSST6q8Q5bi80ZctskRu9fonuu0wi0yPa1lnmkLoQ4fauIc77T4KGgrrSooMzsYImNZlXtbLitGOlJ1tWOvl7vf8A8e6QlsPHEBAQEBAQEBAQEBAQEBAQfP3SpIH3hNgYwNYY2HCAMUmGpJpqa6k7sPFYLaRaZb2OL5a0xxOvhGvhr9oeNBGGDcTvpp4LTtbmnV2/B8LThscUjrPjP8/mzL1gVW3zQtMvBScyx8ldc+enkis282KQ13Ix2nV0/RvlNMP0WO9R9FeHPdp10ydPL9XbW1uY5j1Vo3eTOyQQKLfeYqgICAgICAgICAgICAgICChNM+CCE7+u4OdPI735HOlz3FwFR508gtTLPWYex2feMeSlp8/2ckGFaztIiV/VovyqYUNFhaiNFrgilodL0eik8vwRD5uV42c/2r+Lp7I+8u+ljq9g4yMHm4K9e9Dxrd2XdLeeaICAgICAgICAgICAgICAgoQgjG/oqmX5eQBH7q0rz60vTxRpWEc014guHkaLDLtcF+ekW84KqrYCgsopQoQisw67Ym7nMAtBJpO+VrRuww4Ri8XSPH4VkrHqxPvc32neJzzWPCI+usu0a7txu3NkjJ5BwV696HkXj1Zh263XmiAgICAgICAgICAgICAgIMc8mBpcdwJ/2UTOkaprGs6I7vgaN1LtfmVozL1awjq2RhssoGmOo8QKqlnT9m21w6eUteiq9KBQsBqA5qlWYSjHGyKGwMZm32LGCN7nFrnHzctm0Ry19zist5vny2tvzfro24M2kFUYZdjd8pfFG46ljSedM1u1nWNXm3jS0w2FKogICAgICAgICAgICAgING+nUid3lo/eCx5e7LLhj14cbfjMgVpvTr1hGVteDLJTQOIHhksc7ur4KnJhrHnGvzYCobkKIlcESFES7W4by62yxNce3Y5uq/8AGnBLT4PjDeVFnrbWmnl9pcx2nw3o+Im0bXjX/wCo3+k6ukidlVHly6rZ91bPHyI8nEfRbePuw87L35egrsYgICAgICAgICAgICAgIPOv7+5Pc5vqseXus2Dvw5DaOTDDi4ZrTejTr0RTG+pJ41PiSsbtKxy6R5dFSoXUJRZc1BVBuXXajG9wGkkZaRWnaaRJH/7I2j8RV6T1aHaOLnw6+Nev5T9JlId3TB8eIbwD5hZHKWjR2ezwpZ4+Tj5uK28fdh5uXvy9FXYxAQEBAQEBAQEBAQEBAQaN9NrC/wDD/EFjyd2WTD34cDtzJhsjjyHmaLTl7HCV5skR7YRlDv8A63rG6+N2RQutRKoRC9Flsm49/wDx81MK2d1sXaMUJbvZVvhuWaHH8Xj9HltXylJtyNpZ4v1bT5iq3Kd2HiZO9LeVlBAQEBAQEBAQEBAQEBAQat5isT/hVL92V8ffhG/SM0+wk8JYK/6jVpWe52f+PVG9n08ljdXVlRdjChK9qC5EjtPEeqInZ0+wctHyN4tB9R9Flo5ztfHpeL+cfb+QmK6f7mL9Wz+ELertDlr96W2rKiAgICAgICAgICAgICAg0b5eRE6lKkgcxXOnhXyWPL3ZZMXfhwu2lmdLYZmgEkMDwBmaxuDx/CtOXrcLblyVn2otgOSxuxqvJyRaFm5Qle1BWvopB57J5FEW7r3djpsM5H3mO8wR+avSXkdr01w1t5T9/wDhNt0PrBEcvcaDTiBQ+i36TrWHGZI0tMNxWUEBAQEBAQEBAQEBAQEBB4F/2kOljiBzja6Z9Dpia6JgPMOkP4FgzW8G7w+OYxzfTedI+Gkz8unzeNbLS1rW51xPawU4uNM1rS26VRJa4Oqkljy7Er2imlK9n5UVJdbwuTnw1t7P2YnnJQ2Fp0RKrjSvJESr+SCr/dPI+iFu7LcuS0dXPG79IA8nCn1Uxu0uOpz8PePj8uqXrkvUM0OKM+8N4PEfktml+X3OOy4ebbd1cUgcA5pq0ioI4LaaMxp0XIgQEBAQEBAQEBAQEBBjnmaxrnvIDWtc5xOgaBUnySZ0WrWbWitY6z0Rldd6i0+0TOfWSSSR+AEYmsY0CJlOQPmtGbc06uh47B6CaYYjpWsdfOZ6zPz+xec7CMIdQtls1ojpnVrXtLvCgkChqViXDbQH+12jIj7U1ByzDR9Vjtu6js//ALevx+8vPkKhuqHciVkxy5opeWXeiyrtDyKE7KRPoQeGE+VCjFavNSa+cfk6S67xpbHtxAMcx1SMIFREx9SScj73lxWf1dNdXJ8l5iNKzMecfDpp8U32NgbGwCmTGjLTQLcjZ4kzrLMpQICAgICAgICAgICAg4npSvfqrOIGmj5zR1NRE2hd5mg5VWDPfSunm93sHhfSZ5yztX7zt8t/kiE1Dg9jnMeCCHMNDl6juK04nR1ebhqZq6Wgkva1MoQY3AOqBQsIr7w3ihyypuVotHi8vL2XNetJ197Hjc4uc4kuc4uJcS45nSp1pp4KszrL1cGP0eOKeSxyhlVfuQlglNXAd4Rit3ohnJzRk8Vx08ETOzEw6fCPRGOvgw3faDjccxR5bxyHZoRwyWa2mkPA4fFeJvpGsc0xMbT0npMT5/JOPRdarRLZXOmeXsEpbAXYi8NAzBJzOei2cPdeH2lWtc8xFZjz10j6R02dksrQEBAQEBAQEBAQEBAQQbt1evtNslcDVjD1UfDCwkE+LsR8loZrc1nfdlcN6Dhq1nees/H9tHOOKxPSY3hESqiFg1QXORMtVpq/+uClg/zZ96hl8V50RMsMZ934QpY67Qw2YUfJ8VfMVVrbQ0sdeXLk9/3jV9C7A2XqrvswpQuj608ayEv/AJlu4o0pDkO0snPxV59uny6fk6BZGiICAgICAgICAgICDxdsL29ksksgNHluCL9Y/IHwzP4SqZLctZlvdncN/ccTWk7bz7o3+e3xQM4rzn0FjRI4KCVpKlCjUIHIS1IDV58fyUsFet5bG9Qyr0GCPRnwhSx12j3KMYTI8DVwZTmageimesQwdK5bTPlE/f8AR9OWOERxsYNGMY0cmtA+i9GI0fP7W5rTafFmUqiAgICAgICAgICAgibpcvjHNHZmnKJuOT9Y8ZDwb/GtTiLazyus7A4fkx2yzvbpHuj9Z+yPi9azoNVzSi8SOKEysJRCrUWhR5REtSx+8493qplgxbzLYBzUMq8INeI9lnw/kpndip3a+50+ymzk1pljmjGONlrs0c7R7zWYg8SU3tzIPCnCtMmOk20n2vN7S4qmHmrbe1J09/WNPqnxb7iRAQEBAQEBAQEBAQYbZaWxRvkeaMjY57zwa0Ek+QUTOkayvjpbJaKV3mdI+L5xvS2vtE0kz/fle557qnJvICg8F50zrOr6Hiw1xUrjrtEafz37tYqrMq0omBEiCoRZZKcjyRW2zBYdCe/+vVTLFi2lmChkheEGvAewzl9FM7sOPuV9yUuhmbt2pnFlncPAyA/xBbHDT1mHgf1HXpjt/uj7JSW25cQEBAQEBAQEBAQEHGdK15dVYurBo60SNj78A7T/AJAD8SwZ7aV083tdhYPScTzztWNfjtH6/BCp1Wm7LxCoSsYURC+qLFUFKolZaD2TyRW3dljsfucyVMqYu6ytULwvCDVsx+zbz/NWndr4fw6pC6IpsNtLdz7LIPEPjcPlXzWXh59fT2PL7frrwsT5Wj7SmVbrjhAQEBAQEBAQEBAQQ70uXj1lrZCD2YIhX9ZJ2j+6Gea0uItrbR2PYGDk4eck/wCU/SP31cECsD243UBzKET1WVoUVnpK8FF4lWqJEGO1HslIVv3ZUs+TByr5qZ3Vx9yF4ULrwUGnYz9mO4q1t2tg/Dh2fRtNhvCzfpGVh8Yn/UBXwz68NXtivNwV/ZpP1hO633CiAgICAgICAgICCjnACpyAzPJB8435eBtE885/7sr3N+CtGj9kBebaeaZl9H4bD6HDXH5Rp8fH6vOaVVlhjB7RUq/5EihNlWuRESuqi+oiWK1HsqYUyT6rJSgA7goTEaRECJXNQadhPYPcfqrW3anDfhuk2PnwWyyurpaYQeTnYf5lOOdLQjj683C5I/8AWfp1fRC9F8+EBAQEBAQEBAQEHP7e2/2ewWh4NHOj6plMjikOCo5YifBY8ttKS3+y8PpeLpWdtdZ+HX8kASaLQd8s3IQw4u0jHPeZXBQvPWGJqljhkqoX1Vqi0McueXeEUt16Mryi8iC5hQho2A5OHNWs0+G7sw9S7JsEkbvuSxP/AGXg/RRG+rYyV5sdq+cTH0fTIK9N82VQEBAQEBAQEBAQRt0zW+jLPZwfee+Z/Jgwt+bz5LW4mdodJ/TuHW98vlGnz/4+qJpytV1Mqbgg13HNGK27ODki8T0Y3IrKtUSNduUJieq2E1PiiKzrLM5F5CUFzEIaVi1cPiV7NTh97Q2otDyKpOzapu+mrqn6yGJ/34o3ebQV6dZ1jV83y05Mlq+UzDaUsYgICAgICAgICCDulG3dbeEgrlCyOIcK0xu+b6eC0c1tby7fsTFycJE/+UzP5fk4uQ1WJ6squOnJBrkoxyyxnJE1UehK2qI1YjJmik26stlGZ5eqSvj3ZSoZVSiFzETDSs2TzzPqrTs1cXS8tqE5/JQ2KvobYKfrLvsp4QtZ/pks/lW/h7kOC7TpycXkj2zPz6/m99ZGiICAgICAgICCjnACpyAzPJB81XnazPLLMa1llkkz17TiQPI0XmTOs6vpWHF6LHXH5REfKGgi6rm5/JEKyWUNaCTVznbtAEVius9WpZZa1rxNOSmYa+HJrM6s7wqti0dGvIaK0MFp0YsSljizbsmhPfTy/wCVWWzi2ZAoZAoKsUphps978R9VM7NWnfbWjvFQ2E49Elpx2DD/AJU8zPMiT+dbnDz6mjje3sfLxcz5xE/l+TtVneMICAgICAgICDWvNj3Qytjze6KQM0HbLSBme+iidmTFMRes221jVEd29F9ukymfFZ2ga1655O4YWmlO+vgVp14e3udbn/qHh6/h1m0/KPz+zHe2wlqsfux+0xEVc+FrnOFNQ9mZ8qjkovhtX2tjg+2uFz9L+pPtnWJ+OkfXRyQhDTWtfu005rE9WY03a1vfQV4AnxRTXlibPFLi1lRkQajyWSOsvLtM0w81d3W3ZszLNFHK2WMY42OwvDh7zQd1VE42Cva/L0tT5S0r6ub2dpdJNHQfdDjnwFdSpikq5e1aTGvJ9f2eHYmF7o8YwNleGsNauJIOE4eBIAHNXmkRMxE9YVwcXe/Je1OWlp0iZn9o+70erDatBqA5wqcjkaLBO73cWnJ09q0KF1SgM1QhqDU/EfVWlrV3bTtR3hQ2Ut9Ck9WWqP7r4H/tteP/AJrZ4ad4cv8A1HTS+O3nEx8p/dJa2nNiAgICAgICAgICAgj/AG62GMzvaLExgk7XXRCjOszrjadA/WtaVrrUZ6+XDr1ru97svtf0EeizTM18J35f2+3hCIb/AIXx4mvY9jwRia8FjxnwK1dNJ0l09stcmCb47RMex49qH2dO71Vq95qcRp6HSEp7K/8AS2fuhj+TQrucvGlphyN+3dNb7xZZY6EnC0Bzi1tXBznuJpl2G/vLLja2eY1iJ2SLPsqLHdE8VpMfXukjkifZwWPZOHMMVHnM4HCtRTs171MR6OkzO6/EZo4rNWKVmKxEREa+EfbX7oplNSTuJJz1pVaczq7nFj9HStPKIj5QsYoXhUhDRazVERu12tzPM+qtLDWN3s3Lc89skEUDQ6TC5wDnNZVopWhccznpzStZtOkI4niacNjjJk10106Je6NdmLTYPaDaAwdaIMAY7Eex1la7h74W3hxzTXVynbHaGLi+T0cT6uu/t09s+Tt1neKICAgICAgICAgICAg8m/8AZyx28NFqh6wMJLKPkjIrrmwg7tFW1Ituz4eJy4JmcdtNXHXl0Q3a7E8TWuJoBOFskTmAAbsbCfMrH6KsNr/qfEW6WmJ+Dhtk9p4mu9ltDfZ3xl0cIkoC4NcQGPOgeBQbg6mXBY7U0jWNlozRe8xPSXobFx+0X6x7BURC0TykaBns/UMrzc8nwBWTFE6atbiZjm0h02120EFrl6lksTooS4EF7O3KRQkAnMAEjvqe5VyW5ungzcPT0fra9fs5K1XTE3tdUBw97D4CtFg5Yep/e8RNeWbz9Pvu29j9iIrw9oL55ojG+MN6sMoXOBcagjMaDKiy0wxaNZliy9sZsUVpjiOnv6/V6c3RDMPct8bvjs5Zl+GQp/be1kr/AFFk09bHHz/ZisfRFaSXGW1wsPZ6vqmPkBGdcQdhodNCd6f23tXn+otLaxj1j36fq9a5uiaGGVkk1qNoa12IxmFrI3fouBc6oV68PEb9Wnn7ay5KzWkcuvtnX4T0+zuruuey2avUWeGGupjY1pPMgVKy1rFdoeVkz5Mv4lpn3zMt5WYhAQEBAQEBAQEBAQEBAQeVtPaTHZpHYZHAgMd1LHSPDXkNc4NaCcgSa0y13Kl9eWdGTFpzxqia8dirRe0kXU2f2eOMuD7XaY3xPMf+W2I4XS8akNHA5lY8NbRHVm4m9LTHLv5uzmuKO5bBKbKHuJBfbbS4h1pLGsNZOQ0AGgJOtSsl+aI9VjwxSbf6k9ERF1wPY4nqW9lzj2Z2vDtGtGE8ASQsH+tH8ht6cLb+SzWO6xI9oult4SYpGta0snNkEZbm6QyNoBUa136b1aItbpaFZvjxxFsdp1128E+3LcjLK6VzHOJmMJcDQNb1ULIgG0GhDK57ys8Ro0rXm271VKogICAgICAgICAgICAgICAgICAgICDF7NHWvVsrxwtqoNWUKQQEBAQEBAQEBAQEBB//2Q=='
    },
    {
      title: 'Платье черное ',
      src: 'https://images-na.ssl-images-amazon.com/images/I/71Ms71OIM2L._SX385._SX._UX._SY._UY_.jpg'
    }
  ];

  const { height, width } = Dimensions.get('window');

  return (
    <View>
      <Carousel
        layout="tinder"
        data={entries}
        itemWidth={width}
        itemHeight={height}
        sliderWidth={width}
        sliderHeight={height}
        renderItem={renderItem}
        layoutCardOffset={0}
      />
      <Navigation activeTab="dashboard" onChange={() => {}}/>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    position: 'relative'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 36,
    lineHeight: 42,
    color: white,
    position: 'absolute',
    bottom: 0,
    left: 0,
    paddingLeft: 10,
    backgroundColor: 'rgba(0, 0, 0, .25)',
    letterSpacing: .16
  },
  container: {
    marginTop: 115,
    justifyContent: 'center',
    alignItems: 'center'
  },
  card: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain'
  },
  avatar: {
    width: 86,
    height: 86,
    borderRadius: 43,
    marginTop: -130,
    borderColor: white,
    borderWidth: 3,
    borderStyle: 'solid'
  }
});
