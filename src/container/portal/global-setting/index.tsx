import React, { useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Slider,
  ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

import { Switch } from 'components/switch';
import { Container } from 'components/container';

import { palette } from 'utils/palette';
import { connect } from '../../../store/connect';

type BlockType = {
  title: string,
  children: any
};

function Block({ children, title }: BlockType) {
  return (
    <View style={styles.block}>
      <Text style={styles.title}>{title}</Text>
      <Container classes={styles.section}>
        {children}
      </Container>
    </View>
  );
}

const initialState = {
  activity: true,
  change: false,
  email: false
};

function GlobalSetting() {
  const [settingState, setSetting] = useState(initialState);
  const [value, setValue] = useState(0);

  const { activity, change, email } = settingState;

  const handleChangeSetting = (key: string, value: string | number | boolean) => {
    if (key) {
      setSetting({
        ...settingState,
        [key]: value
      });
    }
  };

  const renderIcon = () => (
    <Icon
      name="chevron-right"
      style={styles.icon}
    />
  );

  return (
    <ScrollView style={styles.container}>
      <Block title="Настройка аккаунта">
        <View style={styles.wrapper}>
          <Text style={styles.subTitle}>Место</Text>
          <Text style={{ ...styles.subTitle, color: palette.yellow }}>
            Моё местоположение
            {renderIcon()}
          </Text>
        </View>
      </Block>

      <Block title="Расстояние">
        <View style={styles.wrapper}>
          <Slider
            step={1}
            style={{ width: '100%' }}
            maximumValue={100}
            onValueChange={value => setValue(value)}
            value={value}
          />
        </View>
      </Block>

      <Block title="Показывать только">
        <View style={styles.wrapper}>
          <Text style={styles.subTitle}>Аксессуары</Text>
          <Switch
            checked={change}
            onChange={checked => handleChangeSetting('activity', checked)}
          />
        </View>

        <View style={styles.wrapper}>
          <Text style={styles.subTitle}>Туфли</Text>
          <Switch
            checked={activity}
            onChange={checked => handleChangeSetting('activity', checked)}
          />
        </View>

        <View style={styles.wrapper}>
          <Text style={styles.subTitle}>Сумки</Text>
          <Switch
            checked={activity}
            onChange={checked => handleChangeSetting('activity', checked)}
          />
        </View>

        <View style={styles.wrapper}>
          <Text style={styles.subTitle}>Платья</Text>
          <Switch
            checked={activity}
            onChange={checked => handleChangeSetting('activity', checked)}
          />
        </View>
      </Block>

      <Block title="Настройка приложения">
        <View style={styles.wrapper}>
          <Text style={styles.subTitle}>Уведомления</Text>
          <Switch
            checked={email}
            onChange={checked => handleChangeSetting('activity', checked)}
          />
        </View>

        <View style={styles.wrapper}>
          <Text style={styles.subTitle}>Email</Text>
          <Text style={{ ...styles.subTitle, color: palette.yellow }}>
            emailmail@gmail.com
            {renderIcon()}
          </Text>
        </View>
      </Block>

      <Block title="Советы">
        <View style={styles.wrapper}>
          <Text style={styles.subTitle}>Связаться с нами</Text>
          <Text>
            {renderIcon()}
          </Text>
        </View>

        <View style={styles.wrapper}>
          <Text style={styles.subTitle}>Помощь и поддержка</Text>
          <Text>
            {renderIcon()}
          </Text>
        </View>
      </Block>

      <Block title="Юредические сведения">
        <View style={styles.wrapper}>
          <Text style={styles.subTitle}>Конфиденциальность</Text>
          <Text>
            {renderIcon()}
          </Text>
        </View>

        <View style={styles.wrapper}>
          <Text style={styles.subTitle}>Соглашение пользователя</Text>
          <Text>
            {renderIcon()}
          </Text>
        </View>
      </Block>
    </ScrollView>
  );
}

export const GlobalSettingPage = connect(GlobalSetting, {
  store: [],
  dispatch: {}
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 24,
  },
  block: {
    marginBottom: 30
  },
  section: {
    backgroundColor: '#fff',
  },
  title: {
    paddingHorizontal: 24,
    fontFamily: palette.base.fontFamily,
    fontWeight: '600',
    fontSize: 14,
    color: 'rgba(0, 0, 0, .54)',
    marginBottom: 12
  },
  subTitle: {
    fontFamily: palette.base.fontFamily,
    fontWeight: '400',
    fontSize: 17,
    lineHeight: 24,
    color: 'rgba(0, 0, 0, .7)',
  },
  wrapper: {
    paddingVertical: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  icon: {
    fontSize: 20,
    width: 40,
    color: '#9D9D9E'
  }
});
