export { GallaryPage } from './gallary';
export { SettingPage } from './setting';
export { AddImagePage } from './add-photo';
export { ChooseImagePage } from './choose-photo';
export { GlobalSettingPage } from './global-setting';
