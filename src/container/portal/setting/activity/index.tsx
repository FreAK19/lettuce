import React, { useState, Fragment } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';

import { MessageList } from './messages-list';

import { palette } from '../../../../utils/palette';

const { lightColor, lightGrey } = palette;

const ACTIVITY_SECTIONS = [
  {
    title: 'Сообщения',
    Component: MessageList
  },
  {
    title: 'Лайки',
    Component: MessageList,
  },
];

export function ActivityPage() {
  const [activeSections, setSections] = useState([]);

  const renderHeader = ({ title }) => {
    return (
      <View style={styles.header}>
        <Text style={styles.headerTitle}>{title}</Text>
      </View>
    );
  };

  const renderContent = ({ Component }) => {
    return (
      <View>
        <Component />
      </View>
    );
  };

  return (
    <Accordion
      sections={ACTIVITY_SECTIONS}
      activeSections={activeSections}
      renderHeader={renderHeader}
      //  @ts-ignore
      renderContent={renderContent}
      onChange={activeSections => setSections(activeSections)}
    />
  );
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: lightGrey,
    paddingVertical: 23,
  },
  headerTitle: {
    color: lightColor,
    fontSize: 14,
    paddingHorizontal: 16
  }
});
