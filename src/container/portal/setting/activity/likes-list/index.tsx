import React, { useEffect } from 'react';
import { FlatList, StyleSheet, Text } from 'react-native';

import { ActivityItem } from './../list-item';

const list = [
  {
    id: 0,
    title: 'Алексей',
    message: 'Мне понравился Ваш товар',
    date: '14:21',
    image: 'https://www.midlandsderm.com/wp-content/uploads/2019/04/Rachel-R.-Person-760x760.jpg'
  }
];

export function LikesList() {

  const fetchLikes = async () => {
    //await something();
  };

  useEffect(() => {
    fetchLikes();
  });

  return (
    <FlatList
      data={list}
      renderItem={({ item }) => <ActivityItem data={item} />}
      keyExtractor={item => item.title}
    />
  );
}
