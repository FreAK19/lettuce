import React, { useState } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

import { palette } from '../../../../../utils/palette';

type ListType = {
  count?: number,
  message: string,
  title: string,
  date: string,
  image: string
};

type Props = {
  data: ListType
};

const { white, lightColor, yellow } = palette;

export function ActivityItem({ data }: Props) {
  const { count, title, date, image, message }: ListType = data;
  const url = { uri: image };

  return (
    <View style={styles.container}>
      <View style={styles.image}>
        <Image
          style={styles.avatar}
          source={url}
        />
      </View>
      <View style={styles.message}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.message}>{message}</Text>
        <Text style={styles.message}>{message}</Text>
        <Text style={styles.message}>{date}</Text>
      </View>
      {
        count && (
          <View style={styles.count}>
            <Text>{count}</Text>
          </View>
        )
      }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: white,
    paddingVertical: 14,
    paddingHorizontal: 16,
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomColor: lightColor
  },
  avatar: {

  },
  title: {
    color: 'rgba(0, 0, 0, 0.87)',
  },
  image: {

  },
  message: {

  },
  count: {
    fontSize: 14,
    paddingHorizontal: 16
  }
});
