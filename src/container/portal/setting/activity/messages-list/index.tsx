import React, { useEffect } from 'react';
import { FlatList } from 'react-native';

import { ActivityItem } from './../list-item';

const list = [
  {
    id: 0,
    count: 1,
    title: 'Андрей',
    message: 'Здравствуйте еще раз',
    date: '14:21',
    image: 'https://www.midlandsderm.com/wp-content/uploads/2019/04/Rachel-R.-Person-760x760.jpg'
  }
];

export function MessageList() {

  const fetchNewMessages = async () => {
    //await something();
  };

  useEffect(() => {
    fetchNewMessages();
  });

  return (
    <FlatList
      data={list}
      renderItem={({ item }) => <ActivityItem data={item} />}
      keyExtractor={item => item.title}
    />
  );
}
