import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';

import { Navigation } from '../../navigation';

import { SelfPage } from './self';
import { ActivityPage } from './activity';
import { StartCardPage } from './start-card';

import { palette } from '../../../utils/palette';

const { lightGrey } = palette;

type NavigationComponentType = {
  [key: string]: any
};

const navigationKeys: NavigationComponentType = {
  account: <SelfPage />,
  dashboard: <StartCardPage />,
  person: <ActivityPage />
};

export function SettingPage() {
  const [activePage, setActivePage]: [string, Function] = useState('account');

  return (
    <View style={styles.container}>
      <Navigation
        onChange={(key: string) => setActivePage(key)}
        activeTab={activePage}
      >
          {
            navigationKeys[activePage]
          }
      </Navigation>
    </View>
  );
}

const styles = StyleSheet.create({
  scroll: {
  },
  container: {
    flex: 1,
    backgroundColor: lightGrey,
  }
});
