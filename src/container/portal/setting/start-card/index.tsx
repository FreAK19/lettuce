import React from 'react';
import { Actions } from 'react-native-router-flux';
import { StyleSheet, View, ScrollView } from 'react-native';

import { Button } from '../../../../components/button';

import { Icon } from './Icon';

import { palette } from '../../../../utils/palette';

export function StartCardPage() {

  return (
    <ScrollView>
      <View style={styles.container}>
        <Icon />
      </View>
      <View style={styles.action}>
        <Button
          outlined
          variant="pink"
          color={palette.pink}
          onClick={() => Actions.gallery()}
          title="Хочу купить "
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: '10%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  action: {
    marginTop: '10%',
    justifyContent: 'center',
    alignItems: 'center'
  }
});
