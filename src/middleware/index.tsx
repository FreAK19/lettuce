import React, { Component, ComponentType } from 'react';
import { StyleSheet } from 'react-native';
import { Scene } from 'react-native-router-flux';

import { palette } from '../utils/palette';

type Props = {
  path: string,
  title?: string,
  leftTitle?: string,
  hideNavBar?: boolean,
  component: ComponentType
};

export class Route extends Component<Props> {

  render() {
    const {
      path,
      component,
      title = '',
      leftTitle = 'Back',
      hideNavBar = false
    } = this.props;

    return (
      <Scene
        type="replace"
        key={path}
        title={title}
        onLeft={() => {}}
        leftTitle={leftTitle}
        component={component}
        hideNavBar={hideNavBar}
        leftButtonTextStyle={styles.title}
        rightButtonTextStyle={styles.title}
        navigationBarStyle={styles.container}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: palette.primary
  },
  title: {
    color: palette.white
  }
});