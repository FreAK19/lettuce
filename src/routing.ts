export const routes = {
  signIn: 'signIn',
  welcome: 'welcome',
  signUp: 'signUp',
  gallery: 'gallery',
  setting: 'setting',
  globalSetting: 'globalSetting',
  chooseImage: 'chooseImage',
  addPhoto: 'addPhoto',
  regConfirm: 'regConfirm',
};
