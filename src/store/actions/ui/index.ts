import * as constants from '../../constants';

export const requestProgramOptions = async (dispatch: Function, key: string) => {
  try {
    return dispatch({
      payload: key,
      field: 'activeDashboardPage',
      type: constants.CHANGE_ACTIVE_PAGE
    });
  } catch (error) {
    console.log(error);
  }
};

export const savePickerImage = async (dispatch: Function, uri: string) => {
  try {
    return dispatch({
      payload: uri,
      field: 'currentDownloadedImageUrl',
      type: constants.SET_PICKER_IMAGE
    });
  } catch (error) {
    console.log(error);
  }
};
