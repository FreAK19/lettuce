import React, { useContext } from 'react';

import { GlobalStore } from './index';
import { StoreType } from './types';

type Options = {
  store?: string[],
  dispatch: Dispatch
};

type Dispatch = {
  [key: string]: Function
};

type GetStoreType = {
  [key: string]: any
};

const defaultOptions: Options = {
  store: [],
  dispatch: {}
};

const getStore = (store: StoreType, storeElements: string[] = []): GetStoreType => {
  const props: GetStoreType = {};
  storeElements.forEach(storeElement => {
    // @ts-ignore
    if (store.state && store.state.hasOwnProperty(storeElement)) {
      // @ts-ignore
      props[storeElement] = store.state[storeElement];
    }
  });
  return props;
};

const isActionFunction = (func: Function): boolean => typeof func === 'function';

const dispatchEvents = (store: StoreType, dispatchOptions: Dispatch): Dispatch => {
  const props: Dispatch = {};
  for(const key in dispatchOptions) {
    if (dispatchOptions.hasOwnProperty(key) && isActionFunction(dispatchOptions[key])) {
      props[key] = dispatchOptions[key].bind(null, store.dispatch);
    }
  }
  return props;
};

export const connect = (WrappedComponent: any, { store: storeElements, dispatch }: Options = defaultOptions) => {
  return function (props: any) {
    // @ts-ignore
    const globalStore: StoreType = useContext(GlobalStore);
    const storeProps = getStore(globalStore, storeElements);
    const dispatchProps: Dispatch = dispatchEvents(globalStore, dispatch);

    const componentProps = {
      ...props,
      ...storeProps,
      ...dispatchProps
    };

    return (
      <WrappedComponent {...componentProps} />
    );
  };
};
