import React, { useReducer, createContext, Context as ContextType } from 'react';

import * as constants from './constants';

import { Store, StoreType, Action, initialState } from './types';

export const GlobalStore: ContextType<Store> = createContext(initialState);

const reducer = (state: Store, action: Action): Store => {
  switch (action && action.type) {
    case constants.SET_PICKER_IMAGE:
    case constants.CHANGE_ACTIVE_PAGE: {
      const localState: Store  = { ...state };

      if (action.field) {
        //  @ts-ignore
        localState[action.field] = action.payload;
      }
      return { ...localState };
    }

    default:
      return state;
  }
};

export const StoreProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const value: StoreType = { state, dispatch };

  return (
    // @ts-ignore
    <GlobalStore.Provider value={value}>{children}</GlobalStore.Provider>
  );
};

export default GlobalStore;
