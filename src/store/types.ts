export type Store = {
  activeDashboardPage: boolean;
  currentDownloadedImageUrl: string;
};

export type StoreType = {
  state: Store;
  dispatch: Function;
};

export type Action = {
  field?: string;
  type: string;
  payload: any;
};

export const initialState: Store = {
  activeDashboardPage: true,
  currentDownloadedImageUrl: ''
};
