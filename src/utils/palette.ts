export const palette = {
  base: {
    fontFamily: 'montserrat-regular',
    bold: 'montserrat-bold'
  },
  white: '#fff',
  primary: '#538e30',
  yellow: '#f08a1c',
  default: '#fff',
  lightGrey: '#eee',
  grey: '#f7f8fA',
  pink: '#e89799',
  darkGrey: 'rgba(0, 0, 0, 0.12)',
  fontColor: 'rgba(0, 0, 0, 0.87)',
  lightColor: 'rgba(0, 0, 0, 0.54)'
};

export const extraButtonStyle = {
  pink: {
    fontSize: 24,
    paddingVertical: 5,
    textTransform: 'uppercase',
    fontWeight: 'bold'
  }
};
